# Hemerotequer

Hemerotequer is a simple tool to subscribe to newspapers, blogs and similar
websites, extract the content of the posts and store them in a local database.
