from collections import defaultdict
from datetime import date
import feedparser
import requests
import re
from bs4 import BeautifulSoup
from urllib.parse import urlparse, unquote

from os import path

# helper for dates
def _parse_isoformat_date(dtstr):
    clean = dtstr.strip()
    year  = int(clean[0:4])
    month = int(clean[5:7])
    day   = int(clean[8:10])
    return date(year, month, day)

class Newspaper():
    name    = 'Generic'
    domain  = 'www.generic.domain'
    lang    = 'es'
    def __init__(self, feed, feedname):
        self.feed     = feed
        self.feedname = feedname

    def get_new_links(self):
        """
        Retrieves new links, defaults to a RSS/Atom feed parser but can be any
        thing. It makes use of self.feed to store the source.
        Only returns links in self.domain to avoid ads.
        """
        feed = feedparser.parse(self.feed)
        links = set()
        for entry in feed["entries"]:
            if "link" not in entry:
                continue
            if urlparse(entry["link"]).netloc != self.domain:
                continue
            links.add(entry["link"])
        return links


    def get_page(self, link):
        r = requests.get(link)
        if r.status_code == 200:
            return r.text
        else:
            raise ConnectionError

    def get_category_from_link(self, url):
        return unquote( urlparse(url).path ).split('/')[1]

    def extract_article_content(self, link):
        """
        Default extract article behaviour.
        Downloads article content using `self.get_page()` and extracts
        BeautifulSoup tags with `self.select_fields()`.
        Raises ValueError if the selection gives all None values.
        Returns article dict.
        """
        page = self.get_page(link)
        data = self.select_fields(page)

        if all( map(lambda x: x is None, data) ):
            raise ValueError

        title, subtitle, content, date = data
        article = defaultdict(lambda: None)
        if title:
            article["title"]    = title.get_text().replace('\xa0',' ').strip()
        if subtitle:
            article["subtitle"] = subtitle.get_text().replace('\xa0',' ').strip()
        if content:
            article["content"]  = "\n\n".join([x.get_text().replace('\xa0',' ').strip() for x in content ])
        if date:
            article["date"]     = _parse_isoformat_date(date)
        article["category"] = self.get_category_from_link(link)
        article["link"]     = link
        article["medium"]   = self.name
        article["feedname"] = self.feedname
        article["lang"]     = self.lang
        return article

    def select_fields(self, page):
        """
        Abstract method.
        It's supposed to return a (title, subtitle, content, date) tuple with
        all the BeautifulSoup selectors of all the pieces of the document.
        `content` is list of elements (mainly `p`s and `h3`s) while the rest
        are just one tag.
        """
        raise NotImplementedError



class ElPais(Newspaper):
    name   = 'El País'
    domain = 'elpais.com'

    def select_fields(self, page):
        title = None
        subtitle = None
        content = None
        date = None

        if page:
            soup = BeautifulSoup(page, 'html.parser')
            title     = soup.find('h1', itemprop=re.compile(r"(headline)"))
            if title is None:
                title = soup.find('h1', id="titulo_noticia")
            subtitle  = soup.find('h2', itemprop=re.compile(r"(alternativeHeadline)"))
            newsbody  = soup.find(id="cuerpo_noticia") or soup.find("div", class_="entry-content__texto")
            if newsbody:
                content = newsbody.find_all(['p', 'h3'])
            newsdate = soup.find(itemprop="datePublished")
            if newsdate and newsdate.has_attr("content"):
                date = newsdate["content"]
            if newsdate and newsdate.has_attr("datetime"):
                date = newsdate["datetime"]

        return (title, subtitle, content, date)

class ElEspañol(Newspaper):
    name = 'El Español'
    domain = 'www.elespanol.com'

    def select_fields(self, page):
        title = None
        subtitle = None
        content = None
        date = None

        if page:
            soup = BeautifulSoup(page, 'html.parser')

            if soup.find('section', id="liveblog"):
                newsbody = soup.find(id="liveblog")
                if newsbody:
                    content = newsbody.find_all(['p', 'h4', 'h3'])
                title    = soup.find(itemprop="headline")
                subtitle = soup.find(itemprop="alternativeHeadline")
                dateraw  = soup.find("meta", property="article:published_time")
                if dateraw.has_attr("content"):
                    date = dateraw["content"]
            else:

                header    = soup.find(class_="article-header")
                title     = header.h1
                subtitle  = header.h2
                newsbody  = soup.find(id="article-body-content")
                if newsbody:
                    content = newsbody.find_all(['p', 'h3'])
                newsdate  = soup.find("time", class_="article-header__time")
                if newsdate and newsdate.has_attr("datetime"):
                    date = newsdate["datetime"]

        return (title, subtitle, content, date)

class ElDiario(Newspaper):
    name = "El Diario"
    domain = "www.eldiario.es"

    def select_fields(self, page):
        title = None
        subtitle = None
        content = None
        date = None

        if page:
            soup = BeautifulSoup(page, 'html.parser')
            article    = soup.find("article", id="content")
            header     = article.header
            title      = header.h1
            subtitle   = header.find("div", class_="subtitle")
            newsbody   = article.find("div", id="edi-body") or article.find("div", class_="pg-body")
            if newsbody:
                content = newsbody.find_all(['p', 'h3'])
            newsdate   = soup.find("time", class_="dateline")
            if newsdate and newsdate.has_attr("datetime"):
                date = newsdate["datetime"]

        return (title, subtitle, content, date)

class LaVanguardia(Newspaper):
    name = 'La Vanguardia'
    domain = 'www.lavanguardia.com'
    def select_fields(self, page):
        title = None
        subtitle = None
        content = None
        date = None

        if page:
            soup = BeautifulSoup(page, 'html.parser')
            article    = soup.find("article", class_="container", itemtype="https://schema.org/NewsArticle")
            header     = article.header
            title      = header.find("h1", itemprop="headline")
            subtitle   = article.find("h2", itemprop="alternativeHeadline")
            newsbody   = article.find("div", itemprop="articleBody")
            if newsbody:
                content = newsbody.find_all(['p', 'h3'])
            newsdate    = soup.find("time", itemprop="datePublished")
            newsdate    = newsdate or soup.find("time", itemprop="dateModified")
            if newsdate and newsdate.has_attr("datetime"):
                date = newsdate["datetime"]

        return (title, subtitle, content, date)

class NYTimes(Newspaper):
    name   = 'New York Times'
    domain = 'www.nytimes.com'
    lang   = 'en'
    def select_fields(self, page):
        title = None
        subtitle = None
        content = None
        date = None

        if page:
            soup = BeautifulSoup(page, 'html.parser')
            article    = soup.find("article")

            titleblock = soup.find("h1", itemprop="headline")
            if titleblock:
                title = titleblock.find("span")

            if article:
                header   = article.find("header")
                if header:
                    subtitle = header.find('p')
                newsbody   = article.find(itemprop="articleBody")
                if newsbody:
                    content = newsbody.find_all(['p', 'h3', 'h2'])

            newsdate    = soup.find("meta", itemprop="datePublished")
            newsdate    = newsdate or soup.find("meta", itemprop="dateModified")
            if newsdate and newsdate.has_attr("content"):
                date = newsdate["content"]

        return (title, subtitle, content, date)

    def get_category_from_link(self, url):
        exploded = unquote( urlparse(url).path ).split('/')
        if len( exploded ) >= 5:
            return exploded[4]

class ElMundo(Newspaper):
    name   = 'El Mundo'
    domain = 'www.elmundo.es'
    def select_fields(self, page):
        title = None
        subtitle = None
        content = None
        date = None

        if page:
            soup = BeautifulSoup(page, 'html.parser')
            title      = soup.find("h1", class_="js-headline")
            subtitle   = soup.find("div", class_="subtitle-items")
            article    = soup.find('article')
            if article:
                newsbody   = article.find("div", class_ = "row content cols-70-30")
                if newsbody:
                    content = newsbody.find_all(['p', 'h3'])

            newsdate    = soup.find("time", class_="date")
            if newsdate and newsdate.has_attr("datetime"):
                date = newsdate["datetime"]

        return (title, subtitle, content, date)

class Público(Newspaper):
    name = 'Público'
    domain = 'www.publico.es'
    def select_fields(self, page):
        title = None
        subtitle = None
        content = None
        date = None

        if page:
            soup = BeautifulSoup(page, 'html.parser')
            head = soup.find(itemprop=re.compile(r"(headline)"))
            if head:
                head.span and head.span.decompose()
                title = head.find('h1')
            subtitle  = soup.find('div', itemprop=re.compile(r"(description)"))
            newsbody  = soup.find(itemprop="articleBody")
            if newsbody:
                content = newsbody.find_all(['p', 'h3'])
            newsdate    = soup.find("time", itemprop="datePublished")
            newsdate    = newsdate or soup.find("time", itemprop="dateModified")
            if newsdate and newsdate.has_attr("datetime"):
                date = newsdate["datetime"]

        return (title, subtitle, content, date)

class LaRazón(Newspaper):
    name = 'La Razón'
    domain = 'www.larazon.es'
    def get_page(self, link):
        r = requests.get(link)
        if r.status_code == 200:
            r.encoding = 'utf8'
            return r.text
        else:
            raise ConnectionError
    def select_fields(self, page):
        title = None
        subtitle = None
        content = None
        date = None

        if page:
            soup = BeautifulSoup(page, 'html.parser')
            title     = soup.find('h1', itemprop=re.compile(r"(headline)"))
            subtitle  = soup.find('div', itemprop=re.compile(r"(news-detail__new__subtitle)"))
            newsbody  = soup.find(itemprop="articleBody")
            if newsbody:
                content = newsbody.find_all(['p', 'h3'])
            newsdate    = soup.find("time", itemprop="datePublished")
            newsdate    = newsdate or soup.find("time", itemprop="dateModified")
            if newsdate and newsdate.has_attr("datetime"):
                date = newsdate["datetime"]

        return (title, subtitle, content, date)




if __name__ == "__main__":

    from pprint import pprint


    # El País
    #pais = ElPais('https://ep00.epimg.net/rss/elpais/portada.xml', 'Portada')
    #pais_cultura = ElPais('https://ep00.epimg.net/rss/cultura/portada.xml', 'Cultura')
    #pprint(pais.extract_article_content('https://elviajero.elpais.com/elviajero/2019/02/15/actualidad/1550231943_050138.html'))
    #pprint(pais.extract_article_content('https://smoda.elpais.com/placeres/lorena-berdun-entrevista-amor-sexo/#?ref=rss&format=simple&link=link%7CEl'))


    # Arco related
    #pprint(pais.extract_article_content('https://elpais.com/economia/2019/02/13/actualidad/1550080413_363722.html#?ref=rss&format=simple&link=link'))
    #pprint(pais.extract_article_content('https://elpais.com/cultura/2019/02/11/actualidad/1549899016_905921.html#?ref=rss&format=simple&link=link'))
    # Se refieren a Perú como: peruano, andino, etc. Tomarlos como referencia
    # para la jerga


    # El Español
    #espanol_portada = ElEspañol('https://www.elespanol.com/rss/', 'Portada')
    #espanol_cultura = ElEspañol('https://www.elespanol.com/rss/cultura', 'Cultura')
    #pprint(espanol_portada.extract_article_content('https://www.elespanol.com/cultura/20190216/berlinale-corona-israeli-nadav-lapid/376712977_0.html'))

    #espanol_portada = ElEspañol('https://www.elespanol.com/rss/', 'Portada')
    #pprint(espanol_portada.extract_article_content('https://www.elespanol.com/opinion/editoriales/20190212/juicio-golpe-verdad-frente-relato/375602438_14.html'))


    # El Diario
    #eldiario = ElDiario('https://www.eldiario.es/rss', 'Portada')
    #pprint(eldiario.extract_article_content('https://www.eldiario.es/politica/Podemos-IU-aliados_0_868263923.html'))
    #pprint(eldiario.extract_article_content('https://www.eldiario.es/letrapequeña_isaacrosa/VuelvePuigdemont-cuento-relato_6_868323187.html'))


    # La Vanguardia
    #vanguardia_portada = LaVanguardia('https://www.lavanguardia.com/feed/rss/home', 'Portada')
    #vanguardia_cultura = LaVanguardia('https://www.lavanguardia.com/feed/rss/cultura', 'Cultura')
    #pprint(vanguardia_cultura.extract_article_content('https://www.lavanguardia.com/cultura/20190211/46379611729/arco-2019-madrid-optimista-riqueza-peru.html'))

    # New York Times
    #nytimes_portada = NYTimes('https://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml', 'Portada')
    #nytimes_arte    = NYTimes('https://rss.nytimes.com/services/xml/rss/nyt/Arts.xml', 'Arte')
    #pprint(nytimes_portada.extract_article_content('https://www.nytimes.com/2019/02/18/us/edray-goins-black-mathematicians.html'))
    #pprint(nytimes_portada.extract_article_content('https://www.nytimes.com/2019/02/18/nyregion/nypd-brian-simonsen-christopher-ransom.html'))


    # El Mundo
    #elmundo_portada = ElMundo('https://e00-elmundo.uecdn.es/rss/portada.xml', 'Portada')
    #elmundo_cultura = ElMundo('https://e00-elmundo.uecdn.es/rss/cultura.xml', 'Cultura')
    #pprint(elmundo_portada.extract_article_content('https://www.elmundo.es/espana/2019/02/19/5c6bcbd1fc6c8313288b4700.html'))
    #pprint(elmundo_portada.extract_article_content('https://www.elmundo.es/cultura/2019/02/18/5c6a6dcffc6c8362788b4603.html'))


    # Público
    #publico_portada = Público('https://www.publico.es/rss/', 'Portada')
    #publico_cultura = Público('https://www.publico.es/rss/culturas', 'Cultura')
    #pprint(publico_portada.extract_article_content('https://www.publico.es/culturas/teatro-pavon-teatro-kamikaze-madrid-anuncia-temporada-sera-ultima-pide-cesion-espacio-publico.html'))
    #pprint(publico_portada.extract_article_content('https://www.publico.es/sociedad/karl-lagerfeld-muere-disenador-karl-lagerfeld.html'))


    # La Razón
    #razon_portada = LaRazón('https://www.larazon.es/rss/portada.xml', 'Portada')
    #razon_cultura = LaRazón('https://www.larazon.es/rss/cultura.xml', 'Cultura')
    #pprint(razon_portada.extract_article_content('https://www.larazon.es/espana/turull-recurre-a-machado-no-se-nos-ha-escuchado-DB22018308'))
