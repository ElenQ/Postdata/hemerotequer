import sqlite3
import feedparser
import time
from pprint import pprint
from newspapers import *


if __name__ == '__main__':
    refresh_time = 60 * 30
    db_file      = "data.sqlite"

    media = (
            ElPais('https://ep00.epimg.net/rss/elpais/portada.xml', 'Portada'),
            ElPais('https://ep00.epimg.net/rss/cultura/portada.xml', 'Cultura'),

            ElEspañol('https://www.elespanol.com/rss/', 'Portada'),
            ElEspañol('https://www.elespanol.com/rss/cultura', 'Cultura'),

            ElDiario('https://www.eldiario.es/rss', 'Portada'),
            ElDiario('https://www.eldiario.es/rss/cultura', 'Cultura'),

            LaVanguardia('https://www.lavanguardia.com/feed/rss/home', 'Portada'),
            LaVanguardia('https://www.lavanguardia.com/feed/rss/cultura', 'Cultura'),

            NYTimes('https://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml', 'Portada'),
            NYTimes('https://rss.nytimes.com/services/xml/rss/nyt/Arts.xml', 'Arte'),

            ElMundo('https://e00-elmundo.uecdn.es/rss/portada.xml', 'Portada'),
            ElMundo('https://e00-elmundo.uecdn.es/rss/cultura.xml', 'Cultura'),

            Público('https://www.publico.es/rss/', 'Portada'),
            Público('https://www.publico.es/rss/culturas', 'Cultura'),

            LaRazón('https://www.larazon.es/rss/portada.xml', 'Portada'),
            LaRazón('https://www.larazon.es/rss/cultura.xml', 'Cultura'),
        )

    conn = sqlite3.connect(db_file)
    cur  = conn.cursor()
    cur.execute("""
        CREATE TABLE IF NOT EXISTS articles(
            title TEXT,
            subtitle TEXT,
            content TEXT,
            date DATE,
            link TEXT,
            medium TEXT,
            category TEXT,
            feedname TEXT,
            lang TEXT,
            PRIMARY KEY (link)
        );
    """)
    conn.commit()

    while True:
        # Do stuff
        for medium in media:
            print("Processing ", medium.name, medium.feedname, flush=True)
            cur.execute("SELECT link FROM articles WHERE medium=?",
                        (medium.name,))
            known_articles = set()
            for i in cur.fetchall():
                known_articles.add(i[0])

            to_process = medium.get_new_links() - known_articles

            articles = []
            for i in to_process:
                try:
                    article =  medium.extract_article_content(i)
                    print( "Processing article..." )
                    print( article["medium"], article["title"] )
                    print( article["link"] )
                    articles.append(article)
                except:
                    continue

            if len(articles) == 0:
                continue
            cur.executemany("""
            INSERT INTO articles
                (title, subtitle, content, date, link, medium, category, feedname, lang)
            VALUES
                (:title, :subtitle, :content, :date, :link, :medium, :category, :feedname, :lang)
            """, articles)
            conn.commit()

        print("Sleeping...")
        time.sleep( refresh_time )
        print("Waking up...")
    conn.close()
