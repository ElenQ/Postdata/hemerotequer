import csv
from newspapers import *
import sqlite3

"""
Helper script to initialize database with a cache in a CSV file
"""

media = (
            ElPais('https://ep00.epimg.net/rss/elpais/portada.xml', 'Portada'),
            ElPais('https://ep00.epimg.net/rss/cultura/portada.xml', 'Cultura'),

            ElEspañol('https://www.elespanol.com/rss/', 'Portada'),
            ElEspañol('https://www.elespanol.com/rss/cultura', 'Cultura'),

            ElDiario('https://www.eldiario.es/rss', 'Portada'),
            ElDiario('https://www.eldiario.es/rss/cultura', 'Cultura'),

            LaVanguardia('https://www.lavanguardia.com/feed/rss/home', 'Portada'),
            LaVanguardia('https://www.lavanguardia.com/feed/rss/cultura', 'Cultura'),
    )

db_file      = "data.sqlite"
conn = sqlite3.connect(db_file)
cur  = conn.cursor()
cur.execute("""
    CREATE TABLE IF NOT EXISTS articles(
        title TEXT,
        subtitle TEXT,
        content TEXT,
        date DATE,
        link TEXT,
        medium TEXT,
        category TEXT,
        feedname TEXT,
        PRIMARY KEY (link)
    );
""")
conn.commit()

with open("cache.csv") as f:
    reader = csv.DictReader(f)
    for r in reader:
        for m in media:
            if m.feed != r["feedurl"]:
                continue

            cur.execute("SELECT link FROM articles WHERE medium=?",
                        (m.name,))
            known_articles = set()
            for i in cur.fetchall():
                known_articles.add(i[0])
            if r["url"] in known_articles:
                continue


            try:
                article = m.extract_article_content( r["url"] )
            except:
                break

            print(article["medium"], article["title"])
            print(article["link"])


            cur.execute("""
            INSERT INTO articles
                (title, subtitle, content, date, link, medium, category, feedname)
            VALUES
                (:title, :subtitle, :content, :date, :link, :medium, :category, :feedname)
            """, article)
            break

    conn.commit()
